import React, {Component} from 'react'
import './todo-list-item.css'

export default class TodoListItem extends Component{

    render(){

        let {
            label,
            onDelete,
            onToggleDone,
            onToggleImportant,
            important,
            done
        } = this.props;

        let classNames = 'todo-list-item';

        if (important) {
            classNames += ' important';
        }

        if(done){
            classNames += ' done'
        }

        return (
            <span className={classNames}>
            <div
                className="todo-list-item-label"
                onClick={onToggleDone}
            >
                 {label}
            </div>

             <button type="button"
                     className="btn btn-outline-success btn-sm float-right"
                    onClick={onToggleImportant}
             >
                <i className="fa fa-exclamation"></i>
            </button>
            <button type="button"
                    className="btn btn-outline-danger btn-sm float-right"
                    onClick={onDelete}
            >
                <i className="fa fa-trash-o"></i>
            </button>
        </span>
        )
    }
}
