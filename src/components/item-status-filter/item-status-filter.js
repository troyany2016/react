import React, {Component} from 'react'
import './item-status-filter.css'

export default class ItemStatusFilter extends Component{

    buttons = [
        {name: 'all', label: 'All'},
        {name: 'active', label: 'Active'},
        {name: 'done', label: 'Doone'}
    ];

    changeFilter = (type) => {
        const {onFilterChange} = this.props;
        this.setState({active: type});
        onFilterChange(type)
    };

    render(){
        const {filter} = this.props;

        const buttons = this.buttons.map(({name, label}) => {
            const isActive =  filter === name
            const clazz =  isActive ? 'btn-info' : 'btn-outline-second';
            return <button className={`btn ${clazz}`} key={name} onClick={() => this.changeFilter(name)}>{label}</button>

        })

        return(
            <div className="btn-group">
                {buttons}
            </div>
        )
    }
}
