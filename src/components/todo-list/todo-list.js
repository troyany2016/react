import React from 'react'
import TodoListItem from '../todo-list-item'
import './todo-list.css'

const TodoList = ({todos, onDeleted, onToggleDone, onToggleImportant}) => {
    const items = todos.map((item) => {
        let {id, ...itemProps} = item;
        return (
            <li className="list-group-item" key={id}>
                <TodoListItem
                    {...itemProps}
                    onDelete={() => onDeleted(id)}
                    onToggleDone={() => onToggleDone(id)}
                    onToggleImportant={() => onToggleImportant(id)}
                />
            </li>
        )
    });

    return (
        <ul className="todo-list list-group">
            {items}
        </ul>
    )
};

export default TodoList;